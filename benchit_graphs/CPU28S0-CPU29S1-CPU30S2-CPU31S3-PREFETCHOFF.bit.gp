#gnuplotfile
set title "arch_x86_64.memory_latency.C.pthread.0.read"
set xlabel "data set size [Byte]"
set xtics ("10k" 1.000000e+04,"" 2.000000e+04 1,"" 3.000000e+04 1,"" 4.000000e+04 1,"" 5.000000e+04 1,"" 6.000000e+04 1,"" 7.000000e+04 1,"" 8.000000e+04 1,"" 9.000000e+04 1,"100k" 1.000000e+05,"" 2.000000e+05 1,"" 3.000000e+05 1,"" 4.000000e+05 1,"" 5.000000e+05 1,"" 6.000000e+05 1,"" 7.000000e+05 1,"" 8.000000e+05 1,"" 9.000000e+05 1,"1M" 1.000000e+06,"" 2.000000e+06 1,"" 3.000000e+06 1,"" 4.000000e+06 1,"" 5.000000e+06 1,"" 6.000000e+06 1,"" 7.000000e+06 1,"" 8.000000e+06 1,"" 9.000000e+06 1,"10M" 1.000000e+07,"" 2.000000e+07 1,"" 3.000000e+07 1,"" 4.000000e+07 1,"" 5.000000e+07 1,"" 6.000000e+07 1,"" 7.000000e+07 1,"" 8.000000e+07 1,"" 9.000000e+07 1,"100M" 1.000000e+08,"" 2.000000e+08 1,"" 3.000000e+08 1,"" 4.000000e+08 1,"" 5.000000e+08 1,"" 6.000000e+08 1,"" 7.000000e+08 1,"" 8.000000e+08 1,"" 9.000000e+08 1)
set logscale x 10
set xrange [1.000000e+04:1.000000e+08]
set ytics ("0 " 0.000000e+00,"100 " 1.000000e+02,"200 " 2.000000e+02,"300 " 3.000000e+02,"400 " 4.000000e+02,"500 " 5.000000e+02,"600 " 6.000000e+02)
set yrange [0.000000e+00:6.000000e+02]
set ylabel "latency [cycles]"
set data style points
set term postscript eps color solid
set output "InX8_2199M__0__2015_11_10__21_24_25.bit.eps"
plot "InX8_2199M__0__2015_11_10__21_24_25.bit" using 1:2 title 'memory latency CPU28 locally (CPU cycles)', "InX8_2199M__0__2015_11_10__21_24_25.bit" using 1:3 title 'memory latency CPU28 accessing CPU29 memory (CPU cycles)', "InX8_2199M__0__2015_11_10__21_24_25.bit" using 1:4 title 'memory latency CPU28 accessing CPU30 memory (CPU cycles)', "InX8_2199M__0__2015_11_10__21_24_25.bit" using 1:5 title 'memory latency CPU28 accessing CPU31 memory (CPU cycles)', "InX8_2199M__0__2015_11_10__21_24_25.bit" using 1:6 title 'memory latency CPU28 locally (time)', "InX8_2199M__0__2015_11_10__21_24_25.bit" using 1:7 title 'memory latency CPU28 accessing CPU29 memory (time)', "InX8_2199M__0__2015_11_10__21_24_25.bit" using 1:8 title 'memory latency CPU28 accessing CPU30 memory (time)', "InX8_2199M__0__2015_11_10__21_24_25.bit" using 1:9 title 'memory latency CPU28 accessing CPU31 memory (time)'